exports.updateDevice = function(snapshot, context, db, admin) {

	console.log("Salvando informacion");

	const idRegistro = context.params.idRegistro;

	const obj = {};
	snapshot.forEach(function(childSnapshot) {
		var key = childSnapshot.key;
		var val = childSnapshot.val();
		obj[key] = val;
    });
    obj.update = new Date();
    
    console.log(obj)
    if (obj.deviceid != ""){

        const dbHistorial = db.doc("/historial/"+idRegistro);
        dbHistorial.set(obj);

        const dbMotos = db.doc("/motos/"+obj.deviceid);
        return dbMotos.set(obj).then(function(){
            var updates = {};
            updates["/motos/"+idRegistro] = null;
            return admin.database().ref().update(updates);
        });
    }else{
        console.log("El dispositivo no tiene deviceid, no se puede guardar.")
        return null
    }
};
