// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const cors = require('cors')({origin: true});

admin.initializeApp(functions.config().firebase);
var db = admin.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);



exports.newLocation = functions.database.ref('/motos/{idRegistro}').onCreate((snapshot, context) => {
    const newLocation = require('./app/realtime/newLocation');
    return newLocation.updateDevice(snapshot, context, db, admin);
});